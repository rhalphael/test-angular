import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from 'src/shared/services/common/alert.service';
import { AuntenticacionService } from '../../../shared/services/auntenticacion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  isLoading$: Observable<boolean>;
  constructor(
    private fb: FormBuilder,
    private AuntenticacionService: AuntenticacionService,
    private AlertService: AlertService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.definirFormulario();
  }

  definirFormulario() {
    this.form = this.fb.group({
      username: ['test', [Validators.required]],
      password: ['12345', [Validators.required, Validators.minLength(2)]],
    });
  }

  get fc() {
    return this.form.controls;
  }

  login() {
    this.submitted = true;
    let username = this.form.controls['username'].value;
    let password = this.form.controls['password'].value;
    try {
      this.AuntenticacionService.login(username, password).subscribe((res) => {
        localStorage.setItem('user', JSON.stringify(res));
        this.router.navigateByUrl('/home/reservas');
      });
    } catch (error) {
      this.submitted = false;
      this.AlertService.warning(error);
    }
  }
}
