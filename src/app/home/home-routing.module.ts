import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { ReservasComponent } from './reservas/reservas.component';

const routes: Routes = [
  { path: '', component:HomeComponent,
  children: [
    
    {
      path: 'reservas',
      children: [
        {
          path: '',
          component: ReservasComponent
        }
      ]
    }
  ]
},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
