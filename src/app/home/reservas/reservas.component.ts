import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Reserva } from 'src/shared/models/reserva';
import { ReservasService } from 'src/shared/services/reservas.service';
import { AlertService } from '../../../shared/services/common/alert.service';

@Component({
  selector: 'reservas',
  templateUrl: './reservas.component.html',
  styleUrls: ['./reservas.component.css'],
})
export class ReservasComponent implements OnInit {
  formReserva: FormGroup;
  idModal: string = 'modalVenta';
  showComponente: boolean = false;

  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  reservas: Reserva[] = [];
  reserva: Reserva = null;

  constructor(
    private fb: FormBuilder,
    private AlertService: AlertService,
    private ReservasService: ReservasService
  ) {}

  ngOnInit(): void {
    this.crearFormulario();
    this.definirTabla();
  }

  abrir(reset = true) {
    if (reset) this.formReserva.reset();
    this.showComponente = false;
    setTimeout(() => {
      this.showComponente = true;
      $(`#modal${this.idModal}`).click();
    }, 200);
  }

  crearFormulario() {
    this.formReserva = this.fb.group({
      id: null,
      origen: ['', Validators.required],
      destino: ['', Validators.required],
      fecha: ['', Validators.required],
    });
  }

  get control() {
    return this.formReserva.controls;
  }

  definirTabla() {
    this.dtOptions = {
      ordering: false,
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {
        this.ReservasService.searchDatatable(dataTablesParameters).subscribe(
          (resp: any) => {
            this.ngOnDestroy();
            this.reservas = resp.data || [];
            callback({
              recordsTotal: resp.total || 0,
              recordsFiltered: resp.total || 0,
              data: [],
            });
          },
          (error) => {
            console.log('error', error);
          }
        );
      },
      searching: true,
      lengthMenu: [5, 10, 15, 25],
      language: {
        emptyTable: 'Sin datos',
        lengthMenu: '_MENU_',
        info: 'De _START_ a _END_ registros de _TOTAL_',
        infoEmpty: '',
        processing: 'procesando...',
        loadingRecords: 'Cargando..',
        search: '',
        searchPlaceholder: 'Filtrar tabla...',
        zeroRecords: '',
        infoFiltered: '',
        paginate: {
          first: 'primero',
          last: 'ultimo',
          next: '<i class="fa fa-chevron-right"></i>',
          previous: '<i class="fa fa-chevron-left"></i>',
        },
      },
    };
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  recargarTabla() {
    if (this.datatableElement) {
      this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.ajax.reload();
      });
    }
  }

  escuchar(evento) {
    if (evento == 'click') {
      this.crearReserva();
    }
  }

  crearReserva() {
    let user: any = JSON.parse(localStorage.getItem('user'));

    let reserva: Reserva = {
      user_id: user.id,
      origen: this.formReserva.controls['origen'].value,
      destino: this.formReserva.controls['destino'].value,
      fecha: this.formReserva.controls['fecha'].value,
    };

    let actualizar = false;
    if (this.control['id'].value) {
      actualizar = true;
      reserva.id = this.control['id'].value;
    }

    this.ReservasService.guardar(reserva, actualizar).subscribe(
      (res) => {
        let msj = actualizar ? 'Registro actulizado' : 'Registro guarado';
        this.AlertService.success(msj);
        this.abrir();
        this.recargarTabla();
      },
      (error) => {
        this.AlertService.success(error);
      }
    );
  }

  eliminarReserva(index) {
    this.AlertService.confirmation('Deseas eliminar el registro?').then(
      (value) => {
        if (value.isConfirmed) {
          this.AlertService.loading('Eliminando registro');
          this.ReservasService.eliminar(index).subscribe((res) => {
            this.recargarTabla();
            this.AlertService.success('Registro eliminado');
          });
        }
      }
    );
  }

  editarReserva(index) {
    let reserva = this.reservas[index];
    this.control['id'].setValue(reserva.id);
    this.control['origen'].setValue(reserva.origen);
    this.control['destino'].setValue(reserva.destino);
    this.control['fecha'].setValue(reserva.fecha);
    this.abrir(false);
  }
}
