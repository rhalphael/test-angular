export const environment = {
  production: false,
  api_url: 'http://localhost:3006/api/sentinel',
  api_core: 'http://localhost:3006/api/core',
  socket_url: 'ws://localhost:3006',
};
