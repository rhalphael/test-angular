import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MenuBuscarPipe } from './pipe/menu-buscar.pipe';
import { DataTablesModule } from 'angular-datatables';
import { TooltipModule } from 'ng2-tooltip-directive';
import { FlexLayoutModule } from '@angular/flex-layout';
import { VModalComponent } from './componentes/v-modal/v-modal.component';
import { TPanelComponent } from './componentes/t-panel/t-panel.component';
import { ReservaComponent } from './componentes/reserva/reserva.component';


const COMPONENTS = [
  MenuBuscarPipe,
  VModalComponent,
  TPanelComponent,
  ReservaComponent
];
 
const MODULES = [
  CommonModule,
  HttpClientModule,
  FormsModule,
  ReactiveFormsModule,
  NgSelectModule,
  DataTablesModule,
  TooltipModule,
  FlexLayoutModule
]; 

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS, MenuBuscarPipe, ReservaComponent],

  exports: [...COMPONENTS, ...MODULES],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    // NO_ERRORS_SCHEMA
  ]
})
export class SharedModule {}
