import { Injectable, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../models/User';


@Injectable({
  providedIn: 'root',
})
export class AuntenticacionService implements OnInit {
  usuarios: User[];

  constructor() {
    this.usuarios = [{ id: 1, username: 'test', password: '12345', rol:'cliente' }];
  }

  ngOnInit(): void {}

  login(username: string, pass: string): Observable<any> {
    let search = this.usuarios.find(
      (e) => e.username == username && e.password == pass
    );
    if(search){
      return  of(search);
    }else{
      throw Error ("Datos incorrectos");
    }

   
    
  }
}
