import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Reserva } from '../models/reserva';

@Injectable({
  providedIn: 'root',
})
export class ReservasService {
  id = 2;
  reservas: Reserva[] = [
    {
      id: 1,
      user_id: 1,
      origen: 'Monteria',
      destino: 'Bogota',
      fecha: '2022-01-21T10:41',
    },
    {
      id: 2,
      user_id: 1,
      origen: 'Bogota',
      destino: 'Medellin',
      fecha: '2022-02-21T10:41',
    },
  ];

  constructor() {}

  searchDatatable(dataTablesParameters: any) {
    let datos = { ...dataTablesParameters };

    let obj = {
      start: datos.start,
      length: datos.length,
      search: datos.search.value,
    };
    let filtro = this.reservas;
    if (obj.search) {
      filtro = this.reservas.filter(
        (e) => e.origen.includes(obj.search) || e.destino.includes(obj.search)
      );
    }
    let StartLength = filtro.slice(obj.start, obj.length + obj.start);

    let res = {
      data: StartLength,
      filtro: StartLength.length,
      total: filtro.length,
    };

    return of(res);
  }

  guardar(reserva: Reserva, actualizar: boolean) {
    if (actualizar) {
      let index = this.reservas.findIndex((e) => e.id == reserva?.id);
      this.reservas[index] = reserva;
    } else {
      this.id++;
      reserva.id = this.id;
      this.reservas.push(reserva);
    }

    return of(this.reservas);
  }

  eliminar(index) {
    this.reservas.splice(index, 1);
    return of(this.reservas);
  }
}
