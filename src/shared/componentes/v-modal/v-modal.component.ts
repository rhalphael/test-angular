import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'vt-modal',
  templateUrl: './v-modal.component.html',
  styleUrls: ['./v-modal.component.scss']
})
export class VModalComponent implements OnInit {

  @Input() idModal: string;
  @Input() titulo: string;

  //Para prevenir cerrar con la tecla ESC o un click fuera del modal
  @Input() esc: boolean = true;
  @Input() backdrop: boolean = true;
  @Input() showCloseButton: boolean = true;
  @Output() closeAction = new EventEmitter();
  @Input() centered: boolean = false;
  @Input() size: string = '';

  constructor() { }

  ngOnInit() { 
    console.log(this.idModal);
    
  }

  defSize() {
    switch (this.size) {
      case 'small':
        return 'modal-sm';
      case 'medium':
        return 'modal-lg';
      case 'large':
        return 'modal-xl';
      default:
        return '';
    }
  }
}
