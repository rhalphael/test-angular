import { Location } from '@angular/common';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 't-panel',
  templateUrl: './t-panel.component.html',
  styleUrls: ['./t-panel.component.scss'],
})
export class TPanelComponent implements OnInit {
  @Input() titulo: string;
  @Input() backMethod: string;
  @Output() clickBack: EventEmitter<any> = new EventEmitter();

  constructor(
    private location: Location,
    private router: Router,
  //  private storeShared$: Store<StateShared>
  ) {}

  ngOnInit() {}

  goBack() {
    switch (this.backMethod) {
      case 'back':
        this.location.back();
        break;
      case 'especifico':
        this.clickBack.emit('');
        break;
      default:
       
        break;
    }
  }
}
