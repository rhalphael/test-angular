import { Component, Input, OnInit, Output ,EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Reserva } from 'src/shared/models/reserva';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent implements OnInit {
  @Input() formReserva: FormGroup;
  @Input() reserva:Reserva=null;

  @Output() EMISOR = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  crear() {
    if (this.EMISOR) this.EMISOR.emit('click');
  }

  get form(){
    return this.formReserva.controls;
  }

 


}
