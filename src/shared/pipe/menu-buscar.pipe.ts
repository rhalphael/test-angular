import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'menuBuscar',
})
export class MenuBuscarPipe implements PipeTransform {
  private data: any;
  transform(value: any[], args: any): unknown { 
    if (args === '' && args.length < 3) return value;
    if (!value) return null;
    if (!args) return value;

    args = args.toLowerCase();
    return value.filter((item) => {
      this.data = item.tittle.toLowerCase().includes(args);
      return this.data;
    });
  }
}
